/**
  * build_page.js
  * Building pages using mustache and markdown
  */

var fs = require("fs");

module.exports = {
	index: function() {
		var posts_dir = "./posts/";
		var files_unsorted = fs.readdirSync(posts_dir);
		var files = files_unsorted.sort(function(a, b) {
			return fs.statSync(posts_dir + a).mtime.getTime() - fs.statSync(posts_dir + b).mtime.getTime();
		});
		
		var posts = [];
		
		for (var i = 0; i < files.length; i++) {
			var tmp_lines = [];

			var url;
			var title;
			var caption;
			var body = "";

			url = files[i].split(/.md|.markdown/g)[0];
			
			fs.readFileSync("./posts/" + files[i]).toString().split('\n').forEach(function (line) {
				tmp_lines.push(line);
			});
			
			title = tmp_lines[0];
			caption = tmp_lines[1];
			
			for (var l = 2; l < tmp_lines.length; l++) {
				if (l == 2) {
					if (tmp_lines[2] != "") {
						body = tmp_lines[2];
					}
				} else {
					body += tmp_lines[l] + "\n";
				}
			}
			
			body = body.slice(0, body.length - 1);
			
			posts.push({
				"title": "<a href='/post/" + url + "' class='posttitle'>" + title +"</a>",
				"caption": caption,
				"body": body
			});
		}
		
		var mustache_json = {
			"posts": posts
		};
	}
}