// app.js
// The databaseless blogging platform.


var connect = require("connect");
var http = require("http");
var markdown = require("node-markdown").Markdown;
var mustache = require("mu2");

var build_page = require("./modules/build_page.js");

var app = connect();
//app.use(connect.static(__dirname + '/public'));
app.use(connect.compress());

app.use(function(req, res) {
	mustache.root = __dirname + "/template";

	if (req.method == 'GET') {
		req_path = req.url.split("/");
		console.log(req_path);
		
		if (req.url == "/") {
			res.writeHead(200, "OK", {'Content-Type': 'text/html'});
			
			
			
			res.end("You're at the front page!", 'utf-8');
		} else {
			if (req_path[1] == "post") {
				res.writeHead(200, "OK", {'Content-Type': 'text/html'});
				res.end("You're at going to read post " + req_path[2], 'utf-8'); // TODO: parse the request and mustache it back!
			}
		}
	}
});

var server = http.createServer(app);
server.listen(8080);