# writer.js

The databaseless blogging platform


## The Main Objective

The main objective of *write.js* is to be a extremely simple blogging platform for developers that love node.js and Markdown. It comes with a extremely simple style that you have all the openness to style as you desire/dream/want, also since this is an open source project you are free to change the main source code, which is well documented, as you desire and submit a pull request so others can enjoy it too.